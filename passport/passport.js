const passport = require('passport');

const LocalStrategy = require('passport-local').Strategy;
const bcrypt = require('bcrypt');
const User = require('../models/User');

/**
 * {
 *  email: 'carlos@upgrade.com',
 *  password: '123456
 * 
 * }
 */

// Funciones necesarias para leer los datos de la sesion y deserializarlo.
passport.serializeUser((user, done) => {
    return done(null, user._id);
});

passport.deserializeUser( async (userId, done) => {
    try {
        const existingUser = await User.findById(userId);
        return done(null, existingUser);
    } catch(error) {
        return done(error);
    }
});

//Son las veces que va a interar el algoritmo volviendo a hashear para mantener la seguridad de nuestra web
const SALT_ROUNDS = 10;

//Vamos a crear una instancia de esta clase y a esta le vamos a mandar dos argumentos, el primero un objeto y el segundo un callback
const registerStrategy = new LocalStrategy({
    usernameField: 'email',
    passwordField: 'password',
    passReqToCallback: true,
}, async (req, email, password, done) => {
    try {
        //Comprobar si el usuario que se está intentando registrar ya existe en nuestra base de datos.
        const previousUser = await User.findOne({ email });
        //Retornar error si el usuario ya existe
        if (previousUser) {
            const error = new Error('User already exists!');
            return done(error);
        }
        //Si no existe el usuario convertiemos la contraseña en un hash para poder guardarla en base de datos y usamos el paquete dcrypt
        const hash = await bcrypt.hash(password, SALT_ROUNDS);
        //Guardar el usuario
        const newUser = new User({
            email,
            password: hash,
        });
        const savedUser = await newUser.save();
        //Devolveremos el usuario
        done(null, savedUser);
    } catch (error) {
        return done(error);
    }
});

const loginStrategy = new LocalStrategy({
    usernameField: 'email',
    passwordField: 'password',
    passReqToCallback: true,
}, async (req, email, password, dones) => {
    try {
        // Comprobar si el usuario existe
        const currentUser = await User.findOne({ email });

        if (!currentUser) {
            const error = new Error('Email or password not valid');
            return done(error);
        }

        // Comprobar la contraseña
        const isValidPassword = await bcrypt.compare(password, currentUser.password);

        if (!isValidPassword) {
            const error = new Error('Email or password not valid');
            return done(error);
        }

        // Contraseña correct -> logeamos al usuario
        return done(null, currentUser);
    } catch (error) {
        return done(error);
    }
})
//Dos parámetros el primero el nombre de la estrategia, y el segundo tenemos que pasarle la estrategia.
passport.use('acceso', loginStrategy);
passport.use('registro', registerStrategy)