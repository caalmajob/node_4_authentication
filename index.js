const express = require('express');
const mongoose = require('mongoose');
const passport = require('passport');
const session = require('express-session');
const MongoStore = require('connect-mongo');
const db = require('./db.js');
const indexRoutes = require('./routes/index.routes');
const authRoutes = require('./routes/auth.routes');
const petRoutes = require('./routes/pets.routes');
const shelterRoutes = require('./routes/shelters.routes');

//Decirle a passport que use la estrategia
require('./passport/passport');

db.connect();
const PORT = 3000;

const app = express();

//Configuración para express-session
app.use(session({
    secret: 'AasQsfi.123-@',
    resave: false,
    saveUninitialized: false,
    cookie: {
        maxAge: 48 * 60 * 60 * 1000
    },
    //Guardar o almacenar la sesión
    store: MongoStore.create({ mongoUrl: db.DB_URL}),
}));

//Decirle a passport que se inicialice de forma interna
app.use(passport.initialize());
app.use(passport.session());




app.use(express.json());
app.use(express.urlencoded({ extended: true}));

app.use('/', indexRoutes);
app.use('/auth', authRoutes);
app.use('/pets', petRoutes);
app.use('/shelters', shelterRoutes);


app.use('*', (req, res, next) => {
    const error = new Error('Route not found');
    error.status = 404;
    next(error);
})

app.use((error, req, res, next) => {
    return res.status(error.status || 500).json(error.message || 'Unexpected error');
});

app.listen(PORT, () => {
    console.log(`Server listening in http://localhost:${PORT}`)
});