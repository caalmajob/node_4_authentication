const express = require('express');
const passport = require('passport');

const router = express.Router();


router.post('/register', (req, res, next) => {
    //Comprobar que tenemos usuario
    const { email, password } = req.body;

    console.log('Registrando usuario', req.body);
    if(!email || !password) {
        const error = new Error('User and Password are required');
        return res.json(error.message);
    } 
    // usamos la estrategia y un callback con dos argumentos, uno en caso de error y otro en caso de éxito.
    passport.authenticate('registro', (error, user) => {
        if(error) {
            return res.send(error.message);
        }

        return res.send(user);
    })(req);
});

router.post('/login', (req, res, next) => {
    const { email, password } = req.body;
    console.log('logeando usuario...', req.body);

    if(!email || !password) {
        const error = new Error('User and Password are required');
        return res.json(error.message);
    }

    passport.authenticate('acceso', (error, user) => {
        if(error) {
            return res.json(error.message);
        }

        req.logIn(user, (error) => {
            if(error) {
                return res.send(error.message);
            }
            
            return res.send(user);
        })
    })(req, res, next)

});

router.post('/logout', (req, res, next) => {
    if(req.user) {
        req.logOut();

        req.session.destroy(() => {
            res.clearCookie('connect.sid');
            return res.json('Usuario deslogeado con éxito');
        })
    } else {
    return res.json('No user found');
}
});

module.exports = router;